export const doctor = {
    info: {
        fio: "Константин Степанов",
        position: "Терапевт"
    },
    soon: [
        {
            time: "15:40",
            fio: "Геннадий Степанович",
            photo: "1.jpg",
        },
        {
            time: "16:20",
            fio: "Виталий Овсянников",
            photo: "2.jpg",
        },
        {
            time: "16:40",
            fio: "Екатерина Сколькина",
            photo: "8.jpg",
        },
        {
            time: "17:40",
            fio: "Пётр Степанович",
            photo: "3.jpg",
        },
        {
            time: "18:00",
            fio: "Анантолий Сергеевич",
            photo: "4.jpg",
        }
    ],
    last: [
        {
            time: "12:00",
            fio: "Андрей Степанович",
            photo: "5.jpg",
        },
        {
            time: "12:20",
            fio: "Дмитриенко Витя",
            photo: "6.jpg",
        },
        {
            time: "14:40",
            fio: "Александр Иванович",
            photo: "7.jpg",
        }
    ]
};