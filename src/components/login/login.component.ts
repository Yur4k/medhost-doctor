import {Component, Input} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";


@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent{
    @Input() cabinet: HTMLElement;
    access: HTMLElement;

    constructor(private authService: AuthService, private router: Router){}

    openCabinet(username: string, password: string) {
        this.authService.login(username, password, (success) => {
            if (success) {
                this.router.navigate(["cabinet"]);
            }
        });
    }
}