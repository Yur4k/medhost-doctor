import { Component } from '@angular/core';
import {doctor} from "../../data/doctor";

@Component({
    selector: 'last-pacients',
    templateUrl: './last-pacients.component.html',
    styleUrls: ['./last-pacients.component.css']
})
export class LastPacientsComponent {
    doctor = doctor;
}