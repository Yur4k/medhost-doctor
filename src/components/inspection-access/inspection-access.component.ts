import {AfterViewInit, Component,} from '@angular/core';
import {HistoryService} from "../../services/history.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'inspection',
    templateUrl: './inspection-access.component.html',
    styleUrls: ['./inspection-access.component.css']
})
export class InspectionAccessComponent implements AfterViewInit{
    private pacientId;

    constructor(private historyService: HistoryService, private router: Router, private route: ActivatedRoute){}

    ngAfterViewInit(): void {
        this.pacientId = this.route.snapshot.params['pacientId'];
    }

    requestCode() {
        this.historyService.requestCode(this.pacientId).subscribe((result) => {
            alert(result.message);
        });
    }

    openHistoryCabinet(key: string) {
        this.historyService.openHistory(this.pacientId, key).subscribe((result) => {
            if (result.success) {
                this.router.navigate(['/inspection', {pacientId: this.pacientId}])
            } else {
                alert(result.message)
            }
        });
    }
}