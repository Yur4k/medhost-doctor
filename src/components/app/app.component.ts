import {Component} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {HelperService} from "../../services/helper.service";
import {InspectionComponent} from "../inspection/inspection.component";
import 'rxjs/add/observable/from';
import {InspectionAccessComponent} from "../inspection-access/inspection-access.component";



@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent{
    constructor(private authService: AuthService, private helper: HelperService){}


    get hideMenu() {
        let currComponent = this.helper.currentComponent;
        return !(currComponent === InspectionComponent || currComponent === InspectionAccessComponent);
    }
}
