import { Component } from '@angular/core';
import {doctor} from "../../data/doctor";


@Component({
    selector: 'soon-pacients',
    templateUrl: './soon-pacients.component.html',
    styleUrls: ['./soon-pacients.component.css']
})
export class SoonPacientsComponent {
    doctor = doctor;
}