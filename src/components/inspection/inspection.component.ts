import {AfterViewInit, Component} from '@angular/core';
import {HistoryService} from "../../services/history.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'inspection',
    templateUrl: './inspection.component.html',
    styleUrls: ['./inspection.component.css']
})
export class InspectionComponent implements AfterViewInit{
    private formTitle: string;
    private formText: string;
    private editableHistoryPage;
    private history_form = [];
    private recipies = [];
    private historyTitle: string;
    private historyBook;
    private activeEditModeType = 0;
    private userId;
    private date = `21/06/2017 | ${new Date().getHours()}`;

    constructor(private historyService: HistoryService, private route: ActivatedRoute, private router: Router){}


    ngAfterViewInit(): void {
        this.userId = this.route.snapshot.params['pacientId'];
    }

    addHistoryPage() {
        let tmpContainer = this.activeEditModeType === 0 ? this.history_form : this.recipies;

        if (this.editableHistoryPage) {
            this.editableHistoryPage.formText = this.formText;
            this.editableHistoryPage.formTitle = this.formTitle;
            this.editableHistoryPage = undefined;
        } else {
            tmpContainer.push({
                formText: this.formText,
                formTitle: this.formTitle,
            });
        }
    }
    closeAndSaveHistory() {
        let historyPage = {
            title: this.historyTitle,
            results: (()=>{
                let tmpArray = [];

                this.history_form.forEach((historyItem) => {
                    tmpArray.push({
                        type: historyItem.formTitle,
                        overview: historyItem.formText
                    });
                });

                return tmpArray;
            })(),
            recipies: (()=>{
                let tmpArray = [];

                this.recipies.forEach((recipie) => {
                    tmpArray.push({
                        desc: recipie.formTitle,
                        recipie: recipie.formText
                    });
                });

                return tmpArray;
            })(),
        };


        this.historyService.addPage(historyPage, this.userId).subscribe((result) => {
            if (!result.success) alert(result.message);

            this.router.navigate(['/cabinet']);
        });
    }
    clearHistoryPage() {
        this.editableHistoryPage = undefined;
        this.formText = "";
        this.formTitle = "";
    }
    editHistoryPage(historyPage) {
        this.formTitle = historyPage.formTitle;
        this.formText = historyPage.formText;
        this.editableHistoryPage = historyPage;
    }
    removeHistoryPage(historyPage, modeType: number) {
        if (historyPage === this.editableHistoryPage) {
            this.editableHistoryPage = undefined;
            this.formText = "";
            this.formTitle = "";
        }
        if (modeType === 0) this.history_form = this.history_form.filter(page => page !== historyPage);
        else this.recipies = this.recipies.filter(recipe => recipe !== historyPage);
    }
    viewInspection(inspectionItem) {
        if (inspectionItem.target.classList.contains('history__annotation'))
        {
            inspectionItem.target.nextElementSibling.classList.toggle('history__visible');
            inspectionItem.target.nextElementSibling.classList.toggle('history__hidden');
        }

    }
    openHistory(history: HTMLElement, openHistoryBtn: HTMLElement) {
        if (!this.historyBook) {
            this.historyService.getHistory(this.userId).subscribe((result) => {
                if (result.success) {
                    this.historyBook = result.history['history_book'];
                } else {
                    alert(result.message);
                }
            });
        }
        history.classList.toggle('history_show');
        openHistoryBtn.classList.toggle('history__close');
    }
    switchEditMode(modeType: number, historySwitch: HTMLElement, recipiesSwitch: HTMLElement) {
        if (modeType === this.activeEditModeType) return;

        historySwitch.classList.toggle('inspection-form__switcher_active');
        recipiesSwitch.classList.toggle('inspection-form__switcher_active');
        this.activeEditModeType = modeType;
    }
}