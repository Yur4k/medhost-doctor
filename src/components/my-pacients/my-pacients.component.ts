import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {PacientService} from "../../services/pacient.service";
import {DateService} from "../../services/date.service";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/fromEvent";

@Component({
    selector: 'my-pacients',
    templateUrl: './my-pacients.component.html',
    styleUrls: ['./my-pacients.component.css']
})
export class MyPacientsComponent implements AfterViewInit{
    private pacientsList;
    @ViewChild("searchInput") searchInput;

    constructor(private pacientService: PacientService){}

    ngAfterViewInit(): void {
        Observable.fromEvent(this.searchInput.nativeElement, "input").map((e: any) => e.target).subscribe((text: any) => {
            this.pacientsList.map((pacient) => {
               pacient.isHide = pacient.fio.indexOf(text.value) === -1;
            });
        });

        this.pacientService.getAllPacients().subscribe((result) => {
            if (result.success) this.pacientsList = result.pacients;
            else alert(result.message);
        });
    }

    getAge(birthdate) {
        return DateService.getYearsOld(birthdate);
    }
}