import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {doctor} from "../../data/doctor";

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit{
    doctor;

    constructor(private authService: AuthService){}

    ngOnInit(): void {
        this.doctor = doctor;
    }
}
