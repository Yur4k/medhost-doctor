import {Routes} from "@angular/router";
import {MainComponent} from "../components/main/main.component";
import {MyPacientsComponent} from "../components/my-pacients/my-pacients.component";
import {SettingsComponent} from "../components/settings/settings.component";
import {SoonPacientsComponent} from "../components/soon-pacients/soon-pacients.component";
import {LastPacientsComponent} from "../components/last-pacients/last-pacients.component";
import {InspectionComponent} from "../components/inspection/inspection.component";
import {GuardService} from "../services/guard.service";
import {LoginComponent} from "../components/login/login.component";
import {InspectionAccessComponent} from "../components/inspection-access/inspection-access.component";

export const appRoutes: Routes = [
    {
        path: "",
        redirectTo: "login",
        pathMatch: "full"
    },
    {
        path: "login",
        component: LoginComponent,
        canActivate: [GuardService],
    },
    {
        path: "cabinet",
        component: MainComponent,
        canActivate: [GuardService]
    },
    {
        path: "my-pacients",
        component: MyPacientsComponent,
        canActivate: [GuardService]
    },
    {
        path: "settings",
        component: SettingsComponent,
        canActivate: [GuardService]
    },
    {
        path: "soon-pacients",
        component: SoonPacientsComponent,
        canActivate: [GuardService]
    },
    {
        path: "last-pacients",
        component: LastPacientsComponent,
        canActivate: [GuardService]
    },
    {
        path: "inspection",
        component: InspectionComponent,
        canActivate: [GuardService]
    },
    {
        path: "inspection-access",
        component: InspectionAccessComponent,
        canActivate: [GuardService]
    }
];