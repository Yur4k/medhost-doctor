import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {AppComponent } from '../components/app/app.component';
import {MainComponent} from "../components/main/main.component";
import {RouterModule} from "@angular/router";
import {appRoutes} from "./router";
import {MyPacientsComponent} from "../components/my-pacients/my-pacients.component";
import {SettingsComponent} from "../components/settings/settings.component";
import {SoonPacientsComponent} from "../components/soon-pacients/soon-pacients.component";
import {LastPacientsComponent} from "../components/last-pacients/last-pacients.component";
import {InspectionComponent} from "../components/inspection/inspection.component";
import {AuthService} from "../services/auth.service";
import {LoginComponent} from "../components/login/login.component";
import {GuardService} from "../services/guard.service";
import {MenuComponent} from "../components/menu/menu.component";
import {HistoryService} from "../services/history.service";
import {PacientService} from "../services/pacient.service";
import {DateService} from "../services/date.service";
import {InspectionAccessComponent} from "../components/inspection-access/inspection-access.component";
import {HelperService} from "../services/helper.service";



@NgModule({
  declarations: [
      AppComponent,
      MainComponent,
      MyPacientsComponent,
      SettingsComponent,
      SoonPacientsComponent,
      LastPacientsComponent,
      InspectionComponent,
      LoginComponent,
      MenuComponent,
      InspectionAccessComponent
  ],
  imports: [
      BrowserModule,
      FormsModule,
      HttpModule,
      RouterModule.forRoot(appRoutes)
  ],
  providers: [AuthService, GuardService, HistoryService, PacientService, DateService, HelperService],
  bootstrap: [AppComponent]
})
export class AppModule { }
