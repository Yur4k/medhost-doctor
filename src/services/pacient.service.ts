import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";



@Injectable()
export class PacientService {
    constructor(private http: Http){}

    getAllPacients(): Observable<any>{
        return this.http.post('http://localhost:3000/pacients/getAll', {}).map(res => res.json());
    }
}
