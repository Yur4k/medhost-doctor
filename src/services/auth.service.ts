import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions} from "@angular/http";
import * as Cookies from "js-cookie";
import "rxjs/add/operator/map";
import 'rxjs/add/observable/of';
import {Observable} from "rxjs/Observable";



@Injectable()
export class AuthService {
    private token;
    private tokenName = "Doctor-Auth-Token";
    user = {logged: false};

    constructor(private http: Http){
        if (Cookies.get(this.tokenName)) {
            this.token = Cookies.get(this.tokenName);
        }

        this.verifyObservable().subscribe((verified) => {
           this.user.logged = verified;
        });
    }

    getAuthHeader() {
        let headers = new Headers();
        headers.append(this.tokenName, this.token);

        return new RequestOptions({headers: headers});
    }

    verifyResponse() {
        let token = Cookies.get(this.tokenName);

        if (token) {
            return this.http.post('http://localhost:3000/auth/validateToken', {token});
        } else {
            return false;
        }
    }

    verifyObservable(): Observable<boolean> {
        let token = Cookies.get(this.tokenName);

        if (token) {
            return this.http.post('http://localhost:3000/auth/validateToken', {token}).map(result => result.json());
        } else {
            return Observable.of(false);
        }
    }

    login(username: string, password: string, callback) {
        return this.http.post('http://localhost:3000/auth/login', {username, password}).map(result => result.json()).subscribe((result) => {
            if (result.success) {
                this.token = result.token;
                this.user.logged = true;
                Cookies.set(this.tokenName, result.token, {
                    expires: 0.5,
                });
                callback(true);
            } else {
                callback(false);
                alert(result.message);
            }
        });
    }

    logout() {

    }
}
