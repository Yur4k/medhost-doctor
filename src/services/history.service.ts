import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {AuthService} from "./auth.service";



@Injectable()
export class HistoryService {

    constructor(private http: Http, private authService: AuthService){}

    requestCode(userId: string): Observable<any> {
        return this.http.post('http://localhost:3000/history/requestCode', {userId}).map(res => res.json());
    }

    openHistory(userId: string, userCode: string): Observable<any> {
        return this.http.post('http://localhost:3000/history/open', {userId, userCode}, this.authService.getAuthHeader()).map(res => res.json());
    }

    getHistory(userId: string) {
        return this.http.post('http://localhost:3000/history/get', {userId}, this.authService.getAuthHeader()).map(res => res.json());
    }

    addPage(historyPage: object, userId: string) {
        return this.http.post('http://localhost:3000/history/save', {historyPage, userId}, this.authService.getAuthHeader()).map(res => res.json());
    }
}
