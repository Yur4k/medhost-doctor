import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "./auth.service";
import {Observable} from "rxjs/Observable";


@Injectable()
export class GuardService implements CanActivate{
    constructor(private authService: AuthService, private router: Router){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        let verifyResponse = this.authService.verifyResponse();
        let isLoginPage = route.url[0].path === "login";

        if (verifyResponse) {
            return verifyResponse.map((response) => {
                let verified = response.json();

                if (verified) {
                    if (isLoginPage) this.router.navigate(['cabinet']);

                    return true;
                } else {
                    this.authService.user.logged = false;

                    if (isLoginPage) return true;
                    else this.router.navigate(['login']);

                }
            });
        } else {
            this.authService.user.logged = false;

            if (!isLoginPage) this.router.navigate(['login']);

            return Observable.of(true);
        }

    }
}