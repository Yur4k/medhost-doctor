import {Injectable} from "@angular/core";
import * as Moment from "moment";


@Injectable()
export class DateService {
    static getYearsOld(birthDate): number {
        return Moment().diff(Moment(birthDate.substring(0, 10), 'YYYY/M/D'), 'years');
    }
}
