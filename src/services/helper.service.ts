import {Component, Injectable} from "@angular/core";
import {ActivatedRoute} from "@angular/router";




@Injectable()
export class HelperService {
    constructor(private activatedRoute: ActivatedRoute) {}

    get currentComponent(): Component {
        if (this.activatedRoute.children.length > 0) {
            return this.activatedRoute.children[0].component;
        } else {
            return this.activatedRoute.component;
        }
    }
}
